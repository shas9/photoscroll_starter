import UIKit

class PhotoCommentViewController: UIViewController {
  //1
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var nameTextField: UITextField!
  
  //2
  var photoName: String?
  var photoIndex: Int!


  override func viewDidLoad() {
    super.viewDidLoad()
    //3
    if let photoName = photoName {
      self.imageView.image = UIImage(named: photoName)
    }
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillShow(_:)),
      name: UIResponder.keyboardWillShowNotification,
      object: nil)

    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillHide(_:)),
      name: UIResponder.keyboardWillHideNotification,
      object: nil)

  }
  
  @IBAction func hideKeyboard(_ sender: AnyObject) {
    print("Hide Keyboard Button Pressed")
    nameTextField.endEditing(true)
    
  }
  //1
  @IBAction func openZoomingController(_ sender: AnyObject) {
    print("Zooming Button pressed")
    self.performSegue(withIdentifier: "zooming", sender: nil)
  }
    
  //2
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let id = segue.identifier,
      let viewController = segue.destination as? ZoomedPhotoViewController,
      id == "zooming" {
      viewController.photoName = photoName
    }
  }

  
  //1
  func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
    guard
      let userInfo = notification.userInfo,
      let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
        as? NSValue
      else {
        return
    }
    
    let adjustmentHeight = (keyboardFrame.cgRectValue.height + 20) * (show ? 1 : -1)
    
    print("\(show) = \(scrollView.contentInset.bottom), \(scrollView.verticalScrollIndicatorInsets.bottom) and adjustment = \(adjustmentHeight)")
    
    scrollView.contentInset.bottom += adjustmentHeight
    scrollView.verticalScrollIndicatorInsets.bottom += adjustmentHeight
  }
    
  //2
  @objc func keyboardWillShow(_ notification: Notification) {
    adjustInsetForKeyboardShow(true, notification: notification)
  }
  @objc func keyboardWillHide(_ notification: Notification) {
    adjustInsetForKeyboardShow(false, notification: notification)
  }

}
